import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello NestJS Dummy Project!';
  }

  getTest(): string {
    return "It's CICD Gitlab Testing";
  }
  getVersion(): string {
    return "This is Project Version!";
  }
}
